package com.uhc.aarp.aces.bookstore.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by palani on 10/17/17.
 */
@RestController
public class BookstoreController {

    boolean valid = true;

    @RequestMapping(value = "/recommended")
    public String readingList() throws InterruptedException {

        String message = null;

        if (valid) {
            valid = false;
            message = "This is GOOD service... ";
        } else {
            valid = true;
            Thread.sleep(200);
            message = "This is BAD service.. ";
        }

        return message;
    }

    @RequestMapping(value = "/retry-recommended")
    public String retryReadingList() throws InterruptedException {

        String message = message = "This is a slow service that needs retry from client... ";

        Thread.sleep(8000);

        return message;
    }

}

